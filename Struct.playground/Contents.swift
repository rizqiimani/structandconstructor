import UIKit

struct DataMahasiswa {
    var NIM: String?
    var nama: String
    var jurusan: String
    var kodeProdi: Int?
    var namaProdi: String
    var alamatMahasiswa: Alamat
}

struct DataDosen {
    var NIP: String
    var nama: String
    var alamatDosen: Alamat
    
}

struct Alamat {
    var alamat: String
    var RT: String
    var RW: String
    var Provinsi: String
}

//var Angkatan2012: [DataMahasiswa] = [DataMahasiswa(NIM: "20120140069", nama: "Rizqi", jurusan: "Teknik", kodeProdi: 014, namaProdi: "TI", alamatMahasiswa: Alamat(alamat: "Tembok Lor", RT: "10", RW: "002", Provinsi: "Jateng")),
//                                     DataMahasiswa(NIM: "20120130001", nama: "Ambar", jurusan: "Teknik", kodeProdi: 013, namaProdi: "Teknik Sipil", alamatMahasiswa: Alamat(alamat: "KK", RT: "01", RW: "004", Provinsi: "Jabar"))]

//Angkatan2012[2].nama = "Ambarukmo"

var Daftar20120: DataMahasiswa = DataMahasiswa(nama: "Rizqi", jurusan: "Teknik", namaProdi: "Teknik Informatika", alamatMahasiswa: Alamat(alamat: "Tegal", RT: "02", RW: "03", Provinsi: "Jawa Tengah"))

//Gimana caranya bikin mana yang required fields mana yang optional fields di dalam Struct nya
//Clue: Pakai Constructor di dalam Struct {}
